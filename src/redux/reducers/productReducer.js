import { ActionsTypes } from "../contants/action-types";

const initialState = {
    products:[]
}

const initialStatePag = {
    pagination:{}
}

export const productReducer = (state = initialState, {type, payload}) => {

    switch (type){
        case ActionsTypes.SET_PRODUCTS:
            return {...state, products: payload};
        default:
            return state;
    }

}


export const selectedProductReducer = (state = {}, {type, payload}) => {

    switch (type){
        case ActionsTypes.SELECTED_PRODUCT:
            return {...state, ...payload};
        default:
            return state;
    }

}


export const paginationReducer = (state = initialStatePag, {type, payload}) => {

    switch (type){
        case ActionsTypes.SET_PAGINATION:
            return {...state, pagination: payload};
        default:
            return state;
    }

}