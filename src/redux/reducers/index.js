import { combineReducers } from "redux";
import { productReducer, selectedProductReducer, paginationReducer } from "./productReducer";

const reducers = combineReducers({
    allProducts: productReducer,
    product: selectedProductReducer,
    pagination: paginationReducer,
})

export default reducers;