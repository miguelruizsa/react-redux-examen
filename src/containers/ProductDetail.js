import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { selectedProduct } from '../redux/actions/productAction';

const ProductDetail = () => {

    const product = useSelector((state) => state.product)
    const {_id, cityid, validdateutc, winddirectioncardinal, probabilityofprecip, relativehumidity, name, longitude, state, lastreporttime, skydescriptionlong, stateabbr, tempc, latitude, iconcode, windspeedkm} = product
    const { productId } = useParams()
    const dispatch = useDispatch()
    console.log(product)

    const fetchProductDetail = async () => {
        const response = await axios.get(`https://api.datos.gob.mx/v1/condiciones-atmosfericas?_id=${productId}`).catch((err) => {
            console.log("Err", err)
        })
        dispatch(selectedProduct(response.data.results[0]))
    }

    useEffect(() => {
        if (productId && productId !== "") fetchProductDetail()
    }, [productId])

    return (
        <div className='container mt-4'>
            <h1>Detalle</h1>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Id:</b> {_id}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>City Id:</b> {cityid}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Validdate Utc:</b> {validdateutc}</label>
                </div>
            </div>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Wind Direction:</b> {winddirectioncardinal}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Probability of Precip:</b> {probabilityofprecip}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Relative Humidity:</b> {relativehumidity}</label>
                </div>
            </div>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Name:</b> {name}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Longitude:</b> {longitude}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>State:</b> {state}</label>
                </div>
            </div>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Last Report Time:</b> {lastreporttime}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Sky Description:</b> {skydescriptionlong}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>State Abbr:</b> {stateabbr}</label>
                </div>
            </div>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Temp c:</b> {tempc}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Latitude:</b> {latitude}</label>
                </div>
                <div className='col-md-4'>
                    <label><b>Icon Code:</b> {iconcode}</label>
                </div>
            </div>

            <div className='row mt-4'>
                <div className='col-md-4'>
                    <label><b>Wind Speedkm:</b> {windspeedkm}</label>
                </div>
            </div>

        </div>
    )

}

export default ProductDetail