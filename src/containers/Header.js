import React from 'react';

const Header = () => {

    return (
        <nav className='navbar navbar-dark bg-dark'>
            <div className='container'>
                <a className='navbar-brand' href='/'>React Redux Examen</a>
            </div>
        </nav>
    )

}

export default Header