import React from 'react';
import { useSelector } from 'react-redux';

const PaginateProducts = () => {

    const pagination = useSelector((state) => state.pagination.pagination)
    const { pageSize, page, total } = pagination;

    let totPags = Math.ceil(total / pageSize)
    let pagAnt = ''
    let sigPag = ''
    let ultPag = '/?page=' + totPags
    let disAnt = 'page-item disabled'
    let disUlt = 'page-item'

    if (page > 1) {
        pagAnt = '/?page=' + (page - 1)
        disAnt = 'page-item'
    }
    if (page < totPags) {
        sigPag = '/?page=' + (page + 1)
    }

    if (page === totPags) disUlt = 'page-item disabled'

    let i = 1
    let to = pageSize

    if (page > 5){

        i = page - 4
        to = page + 5

    }


    const items = (() => {

        const rows = [];
        for (i; i < to; i++) {
            let hr = `/?page=${i}`
            let act =''

            if(i === page) {
                act = "page-item active"
            } else {
                act = "page-item"
            }

            if(i <= totPags)
            rows.push(<li className={act}  key={i}><a className="page-link" href={hr}>{i}</a></li>);
        }
        return rows;

    })



    return (

        <div className='row'>
            <div className='col-md-12'>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className={disAnt}>
                            <a className="page-link" href="/?page=1" aria-label="First">
                                <span aria-hidden="true">&laquo;</span>
                                <span className="sr-only"></span>
                            </a>
                        </li>
                        <li className={disAnt}>
                            <a dis className="page-link" href={pagAnt} aria-label="Previous">
                                <span aria-hidden="true">&lt;</span>
                                <span className="sr-only"></span>
                            </a>
                        </li>

                        {items()}

                        <li className={disUlt}>
                            <a className="page-link" href={sigPag} aria-label="Next">
                                <span aria-hidden="true">&gt;</span>
                                <span className="sr-only"></span>
                            </a>
                        </li>
                        <li className={disUlt}>
                            <a className="page-link" href={ultPag} aria-label="Last">
                                <span aria-hidden="true">&raquo;</span>
                                <span className="sr-only"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className='col-md-12'>Total de registros {total}</div>
        </div>

    )

}

export default PaginateProducts