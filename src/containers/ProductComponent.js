import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Moment from 'moment';

const ProductComponent = () => {

    const products = useSelector((state) => state.allProducts.products)

    const renderList = products.map((product) => {

        const { _id, cityid, name, state, probabilityofprecip, relativehumidity, lastreporttime } = product

        return (
            
            <tr key={_id}>
                <th scope="row">
                <Link to={`/product/${_id}`}>{_id}</Link>
                </th>
                <td>{cityid}</td>
                <td>{name}</td>
                <td>{state}</td>
                <td>{probabilityofprecip}</td>
                <td>{relativehumidity}</td>
                <td>
                    {Moment(lastreporttime).format('YYYY/MM/DD hh:mm:ss a')}
                </td>
                {probabilityofprecip > 60 && relativehumidity > 50 ? <td class="table-success">Si</td> : <td class="table-danger">No</td>}
            </tr>
        )

    })


    return (
        <>
            {renderList}
        </>
    )

}

export default ProductComponent