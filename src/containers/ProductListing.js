import React, { useEffect } from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { setPagination, setProducts } from '../redux/actions/productAction';
import ProductComponent from './ProductComponent';
import PaginateProducts from './PaginateProducts';

const ProductListing = () => {

    const dispatch = useDispatch()
    const dispatchPag = useDispatch()
    const pageSize = 10

    const search = window.location.search
    const params = new URLSearchParams(search)
    let page = params.get('page')

    if( page===null || isNaN(page) ) {
        page=1
    }

    const fetchProducts = async () => {
        const response = await axios
            .get(`https://api.datos.gob.mx/v1/condiciones-atmosfericas?pageSize=${pageSize}&page=${page}`)
            .catch((err) => {
                console.log("Err", err)
            })
        dispatch(setProducts(response.data.results))
        dispatchPag(setPagination(response.data.pagination))
    }

    useEffect(() => {
        fetchProducts()
    }, [])

    return (
        <div className='container mt-4'>
            <h1>Registros</h1>
            <table className="table table-hover table-striped table-bordered">
                <thead >
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">City Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">State</th>
                        <th scope="col">ProbPrec</th>
                        <th scope="col">RelHum</th>
                        <th scope="col">LastRepTime</th>
                        <th scope="col">Llueve</th>
                    </tr>
                </thead>
                <tbody>
                    <ProductComponent />
                </tbody>
            </table>
            <PaginateProducts />
        </div>
    )

}

export default ProductListing